# Initial provisioning

Documentation: https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html

````

# create configmap
ruby process_template.rb configmap-template.yaml red | kubectl apply -f -

# create deployment
ruby process_template.rb deployment-template.yaml red | kubectl apply -f -

# create service

````


# Running on MiniKube

````

http://<minikube ip>:<service port>

````
